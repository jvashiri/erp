(function () {
  owl.Component.env = {};

  /**
   * Symbol used in ComponentWrapper to redirect Owl events to Kwagijima legacy
   * events.
   */
  odoo.widgetSymbol = Symbol("widget");
})();
